const jwt = require("jsonwebtoken");
const db = require("../models");
const User = db.users;

function authenticateToken(req, res, next) {
  const authHeader = req.headers["authorization"];
  const token = authHeader;

  if (token == null) {
    return res.sendStatus(401);
  }

  jwt.verify(token, process.env.SECRET_JWT, (err, decode) => {
    if (err) {
      return res.sendStatus(403);
    }

    req.email = decode.email;

    next();
  });
}

function generateAccessToken(id) {
  return jwt.sign({ id: id }, process.env.SECRET_JWT, {
    expiresIn: "1h",
  });
}

module.exports = {
  authenticateToken,
  generateAccessToken,
};
