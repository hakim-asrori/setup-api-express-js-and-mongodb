module.exports = (app) => {
  require("./user.route")(app);
  require("./auth/login.route")(app);
};
