module.exports = (app) => {
  const users = require("../controllers/user.controller");
  const router = require("express").Router();
  const { authenticateToken } = require("../middlewares/auth.middleware");

  router.get("/", authenticateToken, users.index);
  router.post("/create", authenticateToken, users.create);
  router.get("/:id", authenticateToken, users.show);
  router.put("/:id", authenticateToken, users.update);
  router.delete("/:id", authenticateToken, users.delete);

  app.use("/api/users", router);
};
