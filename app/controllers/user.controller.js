const bcryptjs = require("bcryptjs");
const db = require("../models/");
const User = db.users;
const userService = require("../services/user.service");

exports.index = (req, res, next) => {
  userService.all(req.body, (error, result) => {
    if (error) {
      return next(error.message);
    }

    return res.status(200).send({
      message: "success",
      data: result,
    });
  });
};

exports.create = (req, res, next) => {
  userService.create(req.body, (error, result) => {
    if (error) {
      return next(error.message);
    }

    return res.status(200).send({
      message: "success",
      data: result,
    });
  });
};

exports.show = (req, res, next) => {
  const id = req.params.id;

  userService.show(id, (error, result) => {
    if (error) {
      return next(error.message);
    }

    return res.status(200).send({
      message: "success",
      data: result,
    });
  });
};

exports.update = (req, res, next) => {
  const id = req.params.id;

  userService.update(req.body, id, (error, result) => {
    if (error) {
      return next(error.message);
    }

    return res.status(200).send({
      message: "success",
      data: result,
    });
  });
};

exports.delete = (req, res, next) => {
  const id = req.params.id;

  userService.destroy(id, (error, result) => {
    if (error) {
      return next(error.message);
    }

    return res.status(200).send({
      message: "success",
      data: result,
    });
  });
};
