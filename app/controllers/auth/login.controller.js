const bcryptjs = require("bcryptjs");
const jwt = require("jsonwebtoken");
const db = require("../../models");
const User = db.users;

exports.login = (req, res) => {
  User.findOne({ email: req.body.email })
    .then((result) => {
      checkPassword = bcryptjs.compareSync(req.body.password, result.password);

      if (!checkPassword) {
        res.status(500).send({
          message: "Wrong password",
        });
      }

      let token = jwt.sign(
        {
          email: result.email,
        },
        process.env.SECRET_JWT,
        {
          expiresIn: "1d",
        }
      );

      res.send({
        data: {
          name: result.name,
          email: result.email,
          phone: result.phone,
        },
        message: "Login successfully",
        token: token,
      });
    })
    .catch((err) => {
      res.status(500).send({
        message: err.message || "Some error while retrieving user",
      });
    });
};
