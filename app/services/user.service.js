const bcryptjs = require("bcryptjs");
const db = require("../models/");
const User = db.users;

async function all(attr, callback) {
  User.find()
    .then((result) => {
      return callback(null, result);
    })
    .catch((err) => {
      return callback({
        message: err.message || "some error while retrieving user",
      });
    });
}

async function create({ name, email, phone, password }, callback) {
  const salt = bcryptjs.genSaltSync(10);

  password = bcryptjs.hashSync(password, salt);

  const user = new User({
    name: name,
    email: email,
    phone: phone,
    password: password,
  });

  user
    .save(user)
    .then((result) => {
      return callback(null, result);
    })
    .catch((err) => {
      return callback({
        message: err.message || "some error while create user",
      });
    });
}

async function show(id, callback) {
  User.findById(id)
    .then((result) => {
      return callback(null, result);
    })
    .catch((err) => {
      return callback({
        message: err.message || "some error while show user",
      });
    });
}

async function update(attr, id, callback) {
  const salt = bcryptjs.genSaltSync(10);

  if (attr.password) {
    attr.password = bcryptjs.hashSync(attr.password, salt);
  }

  User.findByIdAndUpdate(id, attr)
    .then((result) => {
      if (!result) {
        return callback({
          message: "user not found",
        });
      }

      return callback(null, result);
    })
    .catch((err) => {
      return callback({
        message: err.message || "some error while update user",
      });
    });
}

async function destroy(id, callback) {
  User.findByIdAndRemove(id)
    .then((result) => {
      if (!result) {
        return callback({
          message: "user not found",
        });
      }

      return callback(null, result);
    })
    .catch((err) => {
      return callback({
        message: err.message || "Some error while delete user",
      });
    });
}

module.exports = {
  all,
  create,
  show,
  update,
  destroy,
};
